package com.findyourservice.web.controller;

import com.findyourservice.model.*;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.JobProfession;
import com.findyourservice.model.enumerations.ServiceType;
import com.findyourservice.model.exceptions.PasswordDoNotMatchException;
import com.findyourservice.model.exceptions.UserNotFoundException;
import com.findyourservice.service.RatingsService;
import com.findyourservice.service.UserService;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    private final RatingsService ratingsService;

    public UserController(UserService userService, RatingsService ratingsService) {
        this.userService = userService;
        this.ratingsService = ratingsService;
    }

    @GetMapping
    @Transactional
    public String getUserPage(@RequestParam(required = false)String error,Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);

        model.addAttribute("bodyContent","user-info");
        return "master-template";
    }

    @GetMapping("/{id}")
    public String getAdvertiserInfoPage(@RequestParam(required = false)String error,
                                        @PathVariable Long id,
                                        Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User advertiser = this.userService.findById(id).orElseThrow(()->new UserNotFoundException(id));
        model.addAttribute("advertiser",advertiser);

        model.addAttribute("bodyContent","advertiser-info");
        return "master-template";
    }
    @GetMapping("/{id}/comments")
    public String getAdvertiserCommentsPage(@RequestParam(required = false)String error,
                                            @PathVariable Long id,
                                            Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User advertiser = this.userService.findById(id).orElseThrow(()->new UserNotFoundException(id));
        model.addAttribute("advertiser",advertiser);

        List<RateComments> rateComments = this.ratingsService.findAllRatesAndCommentGivenToUser(id);
        model.addAttribute("rateComments",rateComments);

        model.addAttribute("bodyContent","advertiser-info-comments");
        return "master-template";
    }
    @GetMapping("/edit-info")
    @Transactional
    public String editUserPage(Model model){
        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        model.addAttribute("bodyContent","edit-user");
        return "master-template";
    }

    @PostMapping("/edit-info")
    @Transactional
    public String editUser(@RequestParam String name,
                           @RequestParam String surname,
                           @RequestParam String phone_number,
                           @RequestParam String city,
                           @RequestParam String country,
                           @RequestParam(required = false)MultipartFile file){
        User user = this.userService.getCurrentlyLoggedInUser();

        this.userService.editUser(user.getId(),name,surname,phone_number,city,country,file);
        return "redirect:/user";
    }

    @GetMapping("/change-password")
    public String changePasswordPage(@RequestParam(required = false) String error,Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);

        model.addAttribute("bodyContent","change-password");
        return "master-template";
    }

    @PostMapping("/change-password")
    public String changePassword(RedirectAttributes redirectAttributes, @RequestParam String oldPassword,
                                 @RequestParam String newPassword,
                                 @RequestParam String repeatedPassword){
        try {
            User user = this.userService.getCurrentlyLoggedInUser();


            this.userService.changePassword(user.getId(),user.getEmail(),oldPassword, newPassword, repeatedPassword);
            redirectAttributes.addFlashAttribute("message","Your password is changed successfully!");
            return "redirect:/user";
        }catch (BadCredentialsException | PasswordDoNotMatchException e){
            return "redirect:/user/change-password?error="+e.getMessage();
        }
    }

    @GetMapping("/current-trips")
    public String getCurrentTripsPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);

        List<Trips> user_trips = this.userService.findAllUserTripsByStatus(user.getId(), AdStatus.ACTIVE);
        model.addAttribute("user_trips",user_trips);

        model.addAttribute("bodyContent","current-trips");
        return "master-template";
    }
    @GetMapping("/current-foods")
    public String getCurrentFoodsPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        List<Food> user_foods = this.userService.findAllUserFoodServicesByStatus(user.getId(),AdStatus.ACTIVE);
        model.addAttribute("user_foods",user_foods);

        model.addAttribute("bodyContent","current-foods");
        return "master-template";
    }
    @GetMapping("/current-educations")
    public String getCurrentEducationsPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        List<Educations> user_educations = this.userService.findAllUserEducationServicesByStatus(user.getId(),AdStatus.ACTIVE);
        model.addAttribute("user_educations",user_educations);

        model.addAttribute("bodyContent","current-educations");
        return "master-template";
    }
    @GetMapping("/current-craftsmen")
    public String getCurrentCraftsmenPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        List<Craftsmen> user_craftsmen = this.userService.findAllUserCraftsmenServicesByStatus(user.getId(),AdStatus.ACTIVE);
        model.addAttribute("user_craftsmen",user_craftsmen);

        model.addAttribute("bodyContent","current-craftsmen");
        return "master-template";
    }

    @GetMapping("/history-trips")
    public String getHistoryTripsPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        List<Trips> user_trips = this.userService.findAllUserTripsByStatus(user.getId(), AdStatus.FINISHED);
        model.addAttribute("user_trips",user_trips);
        List<Long> user_ratings = this.ratingsService.findAllServicesRatedFromUserId(user.getId(), ServiceType.TRIP);
        model.addAttribute("user_ratings",user_ratings);
        model.addAttribute("bodyContent","history-trips");
        return "master-template";
    }
    @GetMapping("/history-foods")
    public String getHistoryFoodsPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        List<Food> user_foods = this.userService.findAllUserFoodServicesByStatus(user.getId(),AdStatus.FINISHED);
        model.addAttribute("user_foods",user_foods);
        List<Long> user_ratings = this.ratingsService.findAllServicesRatedFromUserId(user.getId(), ServiceType.FOOD);
        model.addAttribute("user_ratings",user_ratings);
        model.addAttribute("bodyContent","history-foods");
        return "master-template";
    }
    @GetMapping("/history-educations")
    public String getHistoryEducationsPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        List<Educations> user_educations = this.userService.findAllUserEducationServicesByStatus(user.getId(), AdStatus.FINISHED);
        model.addAttribute("user_educations",user_educations);
        List<Long> user_ratings = this.ratingsService.findAllServicesRatedFromUserId(user.getId(), ServiceType.EDUCATION);
        model.addAttribute("user_ratings",user_ratings);
        model.addAttribute("bodyContent","history-educations");
        return "master-template";
    }
    @GetMapping("/history-craftsmen")
    public String getHistoryCraftsmenPage(Model model){

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user",user);
        List<Craftsmen> user_craftsmen = this.userService.findAllUserCraftsmenServicesByStatus(user.getId(),AdStatus.FINISHED);
        model.addAttribute("user_craftsmen",user_craftsmen);
        List<Long> user_ratings = this.ratingsService.findAllServicesRatedFromUserId(user.getId(), ServiceType.CRAFTSMEN);
        model.addAttribute("user_ratings",user_ratings);
        model.addAttribute("bodyContent","history-craftsmen");
        return "master-template";
    }


    @GetMapping("/change-profession")
    public String changeProfessionPage(@RequestParam(required = false) String error,Model model) {
        if (error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }

        User user = this.userService.getCurrentlyLoggedInUser();

        model.addAttribute("user", user);


        model.addAttribute("bodyContent","change-profession");
        return "master-template";
    }


    @PostMapping("/change-profession")
    public String changeProfession(@RequestParam Long id,
                                   @RequestParam JobProfession profession){
        this.userService.changeProfession(id,profession);
        return "redirect:/user";
    }
}
