package com.findyourservice.web.controller;

import com.findyourservice.model.Food;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.ServicePriceType;
import com.findyourservice.model.exceptions.FoodServiceNotFound;
import com.findyourservice.service.FoodService;
import com.findyourservice.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/foods")
public class FoodController {
    private final UserService userService;
    private final FoodService foodService;

    public FoodController(UserService userService, FoodService foodService) {
        this.userService = userService;
        this.foodService = foodService;
    }

    @GetMapping
    public String foodsPage(@RequestParam(required = false) String error, Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        model.addAttribute("bodyContent","foods");
        model.addAttribute("foods",this.foodService.findAllByStatus(AdStatus.ACTIVE));
        return "master-template";
    }

    @GetMapping("/add")
    public String addFoodServicePage(Model model){
        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        List<String> foodServicePriceTypes = Stream.of(ServicePriceType.values())
                                                    .map(Enum::toString)
                                                    .collect(Collectors.toList());
        model.addAttribute("priceTypes",foodServicePriceTypes);
        model.addAttribute("bodyContent","add-food-service");
        return "master-template";
    }

    @GetMapping("/edit-form/{id}")
    public String editFoodServicePage(@PathVariable Long id, Model model){
        if(this.foodService.findById(id).isPresent()){
            User user = this.userService.getCurrentlyLoggedInUser();
            model.addAttribute("user",user);
            Food food = this.foodService.findById(id).get();

            List<String> foodServicePriceTypes = Stream.of(ServicePriceType.values())
                    .map(Enum::toString)
                    .collect(Collectors.toList());
            model.addAttribute("priceTypes",foodServicePriceTypes);
            model.addAttribute("service",food);
            model.addAttribute("bodyContent","add-food-service");
            return "master-template";
        }

        return "redirect:/foods?error=FoodServiceNotFoundError";
    }

    @PostMapping("/add")
    public String saveFoodService(RedirectAttributes redirectAttributes,
                                  @RequestParam(required = false) Long id,
                                  @RequestParam Long advertiserId,
                                  @RequestParam String advertiserName,
                                  @RequestParam String phoneNumber,
                                  @RequestParam String location,
                                  @RequestParam String foodServiceType,
                                  @RequestParam String description,
                                  @RequestParam String price){

        if(id!=null){
            this.foodService.editFoodService(id,advertiserId, advertiserName, phoneNumber,location,foodServiceType, description, price);
        }else{
            this.foodService.saveFoodService(advertiserId,advertiserName,phoneNumber,location,foodServiceType,description,price);
            redirectAttributes.addFlashAttribute("message","Your service is being reviewed by administrator.");
        }
        return "redirect:/foods";

    }


    @GetMapping("/advertiser-service-info/{id}")
    public String advertiserServiceInfoPage(@PathVariable Long id,Model model){
        model.addAttribute("service",this.foodService.findById(id).get());
        model.addAttribute("bodyContent","foods-advertiser-details");
        return "master-template";
    }
    @GetMapping("/customers-service-info/{id}")
    public String customersSerivceInfoPage(@PathVariable Long id,Model model){
        model.addAttribute("service",this.foodService.findById(id).get());
        model.addAttribute("bodyContent","foods-customer-details");
        return "master-template";
    }

    @GetMapping("/add-client/{id}")
    public String addClientPage(@PathVariable Long id, Model model){

        User user = this.userService.getCurrentlyLoggedInUser();
        Food food = this.foodService.findById(id).get();
        if(user.getId().equals(food.getAdvertiserId())){
            model.addAttribute("bodyContent","add-food-client");
            model.addAttribute("allUsers",this.userService.findAll());
            model.addAttribute("foodServiceId",id);
        }else{
            model.addAttribute("bodyContent","foods");

        }


        return "master-template";

    }

    @PostMapping("/add-client")
    public String addClient(@RequestParam Long foodServiceId,
                            @RequestParam Long clientId){
        Food food = this.foodService.findById(foodServiceId).orElseThrow(()->new FoodServiceNotFound(foodServiceId));

        if(food.getFoodClients().stream().noneMatch(u-> u.getId().equals(clientId))){
            this.foodService.addClient(foodServiceId,clientId);
            return "redirect:/foods";
        }else{
            return "redirect:/add-client/"+foodServiceId+"?error=UserAlreadyInThisService";
        }
    }

    @PostMapping("/finish-food-service/{id}")
    public String finishService(@PathVariable Long id){
        this.foodService.finishService(id);
        return "redirect:/user/history-foods";
    }

    @PostMapping("/delete/{id}")
    @Transactional
    public String deleteFoodService(@PathVariable Long id){

        User user = this.userService.getCurrentlyLoggedInUser();
        Food food = this.foodService.findById(id).get();

        if(user.getId().equals(food.getAdvertiserId()) ||
                user.getRole().toString().equals("ROLE_ADMIN")) {
            this.foodService.deleteById(id);
        }
        return "redirect:/foods";
    }


    @PostMapping("/delete-client")
    public String deleteClient(@RequestParam Long id,
                               @RequestParam Long advertiserId,
                               @RequestParam Long clientId){

        User user = this.userService.getCurrentlyLoggedInUser();
        if(user.getId().equals(advertiserId)){
            this.foodService.deleteClient(id, clientId);
        }
        return "redirect:/foods/advertiser-service-info/"+id;

    }
}
