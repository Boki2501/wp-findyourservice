package com.findyourservice.web.controller;


import com.findyourservice.model.Craftsmen;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.ServicePriceType;
import com.findyourservice.model.exceptions.CraftsmenServiceNotFound;
import com.findyourservice.service.CraftsmenService;
import com.findyourservice.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/craftsmen")
public class CraftsmenController {
    private final UserService userService;
    private final CraftsmenService craftsmenService;

    public CraftsmenController(UserService userService, CraftsmenService craftsmenService) {
        this.userService = userService;
        this.craftsmenService = craftsmenService;
    }


    @GetMapping
    public String craftsmenPage(@RequestParam(required = false) String error, Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        model.addAttribute("bodyContent","craftsmen");
        model.addAttribute("craftsmen",this.craftsmenService.findAllByStatus(AdStatus.ACTIVE));
        return "master-template";
    }

    @GetMapping("/add")
    public String addCraftsmenServicePage(Model model){
        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        List<String> servicePriceTypes = Stream.of(ServicePriceType.values())
                .map(Enum::toString)
                .collect(Collectors.toList());
        model.addAttribute("priceTypes",servicePriceTypes);
        model.addAttribute("bodyContent","add-craftsmen-service");
        return "master-template";
    }

    @GetMapping("/edit-form/{id}")
    public String editFoodServicePage(@PathVariable Long id, Model model){
        if(this.craftsmenService.findById(id).isPresent()){
            User user = this.userService.getCurrentlyLoggedInUser();
            model.addAttribute("user",user);

            Craftsmen craftsmen = this.craftsmenService.findById(id).get();

            List<String> servicePriceTypes = Stream.of(ServicePriceType.values())
                    .map(Enum::toString)
                    .collect(Collectors.toList());

            model.addAttribute("priceTypes",servicePriceTypes);
            model.addAttribute("service",craftsmen);
            model.addAttribute("bodyContent","add-craftsmen-service");
            return "master-template";
        }

        return "redirect:/craftsmen?error=CraftsmenServiceNotFoundError";
    }

    @PostMapping("/add")
    public String saveFoodService(RedirectAttributes redirectAttributes,
                                  @RequestParam(required = false) Long id,
                                  @RequestParam Long advertiserId,
                                  @RequestParam String advertiserName,
                                  @RequestParam String phoneNumber,
                                  @RequestParam String location,
                                  @RequestParam String profession,
                                  @RequestParam String description,
                                  @RequestParam(required = false) String price){

        if(id!=null){
            this.craftsmenService.editCraftsmenService(id,advertiserId,advertiserName,phoneNumber,location,profession,description,price);
        }else{
            this.craftsmenService.saveCraftsmenService(advertiserId,advertiserName,phoneNumber,location,profession,description,price);
            redirectAttributes.addFlashAttribute("message","Your service is being reviewed by administrator.");
        }
        return "redirect:/craftsmen";

    }


    @GetMapping("/advertiser-service-info/{id}")
    public String advertiserServiceInfoPage(@PathVariable Long id,Model model){
        model.addAttribute("service",this.craftsmenService.findById(id).get());
        model.addAttribute("bodyContent","craftsmen-advertiser-details");
        return "master-template";
    }
    @GetMapping("/customers-service-info/{id}")
    public String customersSerivceInfoPage(@PathVariable Long id,Model model){
        model.addAttribute("service",this.craftsmenService.findById(id).get());
        model.addAttribute("bodyContent","craftsmen-customer-details");
        return "master-template";
    }

    @GetMapping("/add-client/{id}")
    public String addClientPage(@PathVariable Long id, Model model){


        User user = this.userService.getCurrentlyLoggedInUser();
        Craftsmen craftsmen = this.craftsmenService.findById(id).get();
        if(user.getId().equals(craftsmen.getAdvertiserId())){
            model.addAttribute("bodyContent","add-craftsmen-client");
            model.addAttribute("allUsers",this.userService.findAll());
            model.addAttribute("craftsmenServiceId",id);
        }else{
            model.addAttribute("bodyContent","craftsmen");
        }


        return "master-template";

    }

    @PostMapping("/add-client")
    public String addClient(@RequestParam Long craftsmenServiceId,
                            @RequestParam Long clientId){
        Craftsmen craftsmen = this.craftsmenService.findById(craftsmenServiceId).orElseThrow(()->new CraftsmenServiceNotFound(craftsmenServiceId));

        if(craftsmen.getCraftsmenServiceClients().stream().noneMatch(u-> u.getId().equals(clientId))){
            this.craftsmenService.addClient(craftsmenServiceId,clientId);
            return "redirect:/craftsmen";
        }else{
            return "redirect:/add-client/"+craftsmenServiceId+"?error=UserAlreadyInThisService";
        }
    }

    @PostMapping("/finish-craftsmen-service/{id}")
    public String finishService(@PathVariable Long id){
        this.craftsmenService.finishService(id);
        return "redirect:/user/history-craftsmen";
    }

    @PostMapping("/delete/{id}")
    @Transactional
    public String deleteCraftsmenService(@PathVariable Long id){

        User user = this.userService.getCurrentlyLoggedInUser();
        Craftsmen craftsmen = this.craftsmenService.findById(id).get();

        if(user.getId().equals(craftsmen.getAdvertiserId()) ||
                user.getRole().toString().equals("ROLE_ADMIN")) {
            this.craftsmenService.deleteById(id);
        }
        return "redirect:/craftsmen";
    }


    @PostMapping("/delete-client")
    public String deleteClient(@RequestParam Long id,
                               @RequestParam Long advertiserId,
                               @RequestParam Long clientId){

        User user = this.userService.getCurrentlyLoggedInUser();
        if(user.getId().equals(advertiserId)){
            this.craftsmenService.deleteClient(id, clientId);
        }
        return "redirect:/craftsmen/advertiser-service-info/"+id;
    }

}
