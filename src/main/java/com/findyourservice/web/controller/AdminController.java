package com.findyourservice.web.controller;

import com.findyourservice.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = {"/admin"})
public class AdminController {

    private final UserService userService;
    private final TripsService tripsService;
    private final FoodService foodService;
    private final EducationsService educationsService;
    private final CraftsmenService craftsmenService;


    public AdminController(UserService userService, TripsService tripsService, FoodService foodService, EducationsService educationsService, CraftsmenService craftsmenService) {
        this.userService = userService;
        this.tripsService = tripsService;
        this.foodService = foodService;
        this.educationsService = educationsService;
        this.craftsmenService = craftsmenService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getAdminPage(Model model){
        model.addAttribute("bodyContent","admin_page");
        return "master-template";
    }

    @GetMapping("/user_approval")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getUserApprovalPendings(Model model){
        model.addAttribute("user_pending_approvals",userService.findAllApprovalPendingUsers());
        model.addAttribute("bodyContent","admin_pending_users");
        return "master-template";
    }

    @GetMapping("/user_overview")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getUserOverviewPage(Model model){
        model.addAttribute("user_list",this.userService.findAll());
        model.addAttribute("bodyContent","admin_users_overview");
        return "master-template";
    }

    @GetMapping("/trips_approval")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getTripsApprovalPendings(Model model){
        model.addAttribute("trip_pending_approvals",this.tripsService.findAllApprovalPendingTrips());
        model.addAttribute("bodyContent","admin_pending_trips");
        return "master-template";
    }

    @GetMapping("/food_services_approval")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getFoodServicesApprovalPendings(Model model){
        model.addAttribute("food_pending_approvals",this.foodService.findAllApprovalPendingFoodServices());
        model.addAttribute("bodyContent","admin_pending_food_services");
        return "master-template";
    }

    @GetMapping("/education_services_approval")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getEducationServicesApprovalPendings(Model model){
        model.addAttribute("education_pending_approvals",this.educationsService.findAllApprovalPendingEducationServices());
        model.addAttribute("bodyContent","admin_pending_education_services");
        return "master-template";
    }

    @GetMapping("/craftsmen_services_approval")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getCraftsmenServicesApprovalPendings(Model model){
        model.addAttribute("craftsmen_pending_approvals",this.craftsmenService.findAllApprovalPendingCraftsmenServices());
        model.addAttribute("bodyContent","admin_pending_craftsmen_services");
        return "master-template";
    }

    @GetMapping("/give-role-to-user")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getGiveRoleToUserPAge(Model model){
        model.addAttribute("allUsers",this.userService.findAll());
        model.addAttribute("bodyContent","admin_give_role_to_user");
        return "master-template";
    }


    @PostMapping("/user_approval/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String deleteUser(@PathVariable Long id){
        userService.deleteById(id);
        return "redirect:/admin/user_approval";
    }

    @PostMapping("/user_overview/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String deleteExistingUser(@PathVariable Long id){
        userService.deleteById(id);
        return "redirect:/admin/user_overview";
    }

    @PostMapping("/user_approval/approve/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String approveUser(@PathVariable Long id){
        userService.approveUser(id);
        return "redirect:/admin/user_approval";
    }

    @PostMapping("/trips_approval/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String deleteTripService(@PathVariable Long id){
        this.tripsService.deleteById(id);
        return "redirect:/admin/trips_approval";
    }

    @PostMapping("/trips_approval/approve/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String approveTripService(@PathVariable Long id){
        this.tripsService.approveTrip(id);
        return "redirect:/admin/trips_approval";
    }

    @PostMapping("/food_services_approval/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String deleteFoodService(@PathVariable Long id){
        this.foodService.deleteById(id);
        return "redirect:/admin/food_services_approval";
    }

    @PostMapping("/food_services_approval/approve/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String approveFoodService(@PathVariable Long id){
        this.foodService.approveService(id);
        return "redirect:/admin/food_services_approval";
    }

    @PostMapping("/education_services_approval/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String deleteEducationService(@PathVariable Long id){
        this.educationsService.deleteById(id);
        return "redirect:/admin/education_services_approval";
    }

    @PostMapping("/education_services_approval/approve/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String approveEducationService(@PathVariable Long id){
        this.educationsService.approveService(id);
        return "redirect:/admin/education_services_approval";
    }

    @PostMapping("/craftsmen_services_approval/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String deleteCraftsmenService(@PathVariable Long id){
        this.craftsmenService.deleteById(id);
        return "redirect:/admin/craftsmen_services_approval";
    }

    @PostMapping("/craftsmen_services_approval/approve/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String approveCraftsmenService(@PathVariable Long id){
        this.craftsmenService.approveService(id);
        return "redirect:/admin/craftsmen_services_approval";
    }

    @PostMapping("/give-role-to-user")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String giveRoleToUser(@RequestParam Long id){
        this.userService.giveRoleToUser(id);
        return "redirect:/admin";
    }
}
