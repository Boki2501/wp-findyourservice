package com.findyourservice.web.controller;

import com.findyourservice.model.Food;
import com.findyourservice.model.Trips;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.exceptions.FreeSlotsException;
import com.findyourservice.service.TripsService;
import com.findyourservice.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;

@Controller
@RequestMapping("/trips")
public class TripsController {
    private final TripsService tripsService;
    private final UserService userService;

    public TripsController(TripsService tripsService, UserService userService) {
        this.tripsService = tripsService;
        this.userService = userService;
    }

    @GetMapping
    public String tripsPage(@RequestParam(required = false) String error,Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        model.addAttribute("bodyContent","trips");
        model.addAttribute("trips",this.tripsService.findAllByStatus(AdStatus.ACTIVE));
        return "master-template";
    }

    @GetMapping("/add")
    public String addTripPage(Model model){
        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        model.addAttribute("bodyContent","add-trip");
        return "master-template";
    }

    @GetMapping("/add-passenger/{id}")
    public String addPassenger(@PathVariable Long id, Model model){


            User user = this.userService.getCurrentlyLoggedInUser();
            Trips trip = this.tripsService.findById(id).get();
            if(user.getId().equals(trip.getDriverId())){
                model.addAttribute("bodyContent","add-passenger");
                model.addAttribute("allUsers",this.userService.findAll());
                model.addAttribute("tripId",id);
            }else{
                model.addAttribute("bodyContent","trips");
            }


        return "master-template";

    }

    @PostMapping("/add-passenger")
    public String addPassenger(@RequestParam Long tripId,
                               @RequestParam Long passengerId){
        Trips trip = this.tripsService.findById(tripId).get();
        if(trip.getPassengers().stream().noneMatch(u-> u.getId().equals(passengerId))){

            try{
                this.tripsService.addPassenger(tripId,passengerId);
            }catch (FreeSlotsException e){
                return "redirect:/trips?"+e.getMessage();
            }

            return "redirect:/trips";
        }else{
            return "redirect:/add-passenger/"+tripId+"?error=UserAlreadyInThisTrip";
        }
    }

    @PostMapping("/add")
    public String saveTrip(RedirectAttributes redirectAttributes,
                           @RequestParam(required = false) Long id,
                           @RequestParam Long driverId,
                           @RequestParam String driverName,
                           @RequestParam String phoneNumber,
                           @RequestParam String fromLocation,
                           @RequestParam String toLocation,
                           @RequestParam String departureDate,
                           @RequestParam String departureTime,
                           @RequestParam String description,
                           @RequestParam int price,
                           @RequestParam int freeSlots){
        if(id!=null){
            this.tripsService.editTrip(id,driverId, driverName, phoneNumber, fromLocation, toLocation, description, price, freeSlots, departureDate, departureTime);
        }else{
            this.tripsService.saveTrip(driverId,driverName,phoneNumber,fromLocation,toLocation,description,price,freeSlots,departureDate,departureTime);
            redirectAttributes.addFlashAttribute("message","Your service is being reviewed by administrator.");
        }
        return "redirect:/trips";
    }

    @GetMapping("/edit-form/{id}")
    public String editTripPage(@PathVariable Long id,Model model){

        if(this.tripsService.findById(id).isPresent()){
            User user = this.userService.getCurrentlyLoggedInUser();
            model.addAttribute("user",user);
            Trips trip = this.tripsService.findById(id).get();
            model.addAttribute("trip",trip);
            model.addAttribute("bodyContent","add-trip");
            return "master-template";
        }

        return "redirect:/trips?error=TripNotFoundError";
    }


    @GetMapping("/driver-trip-info/{id}")
    public String driverTripDetailsPage(@PathVariable Long id,Model model){
        model.addAttribute("trip",this.tripsService.findById(id).get());
        model.addAttribute("bodyContent","trips-driver-details");
        return "master-template";
    }


    @GetMapping("/customers-trip-info/{id}")
    public String customerTripDetailsPage(@PathVariable Long id,Model model){
        model.addAttribute("trip",this.tripsService.findById(id).get());
        model.addAttribute("bodyContent","trips-customer-details");
        return "master-template";
    }


    @PostMapping("/delete-passenger")
    public String deletePassenger(@RequestParam Long id,
                                  @RequestParam Long driverId,
                                  @RequestParam Long passengerId){

        User user = this.userService.getCurrentlyLoggedInUser();
            if(user.getId().equals(driverId)){
                this.tripsService.deletePassenger(id, passengerId);
            }

        return "redirect:/trips/driver-trip-info/"+id;

    }


    @PostMapping("/delete/{id}")
    @Transactional
    public String deleteTrip(@PathVariable Long id){
        User user = this.userService.getCurrentlyLoggedInUser();
        Trips trip = this.tripsService.findById(id).get();

        if(user.getId().equals(trip.getDriverId()) ||
                user.getRole().toString().equals("ROLE_ADMIN")) {
            this.tripsService.deleteById(id);
        }
        return "redirect:/trips";
    }





    @PostMapping("/finish-trip/{id}")
    public String finishTrip(@PathVariable Long id){
        this.tripsService.finishTrip(id);
        return "redirect:/user/history-trips";
    }

}
