package com.findyourservice.web.controller;

import com.findyourservice.model.enumerations.JobProfession;
import com.findyourservice.model.enumerations.Role;
import com.findyourservice.model.exceptions.EmailExistsException;
import com.findyourservice.model.exceptions.InvalidArgumentException;
import com.findyourservice.model.exceptions.PasswordDoNotMatchException;
import com.findyourservice.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/register")
public class RegisterController {
    private final UserService userService;

    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getRegisterPage(@RequestParam(required = false) String error, Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }
        model.addAttribute("bodyContent","register");
        return "master-template";

    }

    @PostMapping
    public String register(@RequestParam String email,
                           @RequestParam String password,
                           @RequestParam String repeatedPassword,
                           @RequestParam String name,
                           @RequestParam String surname,
                           @RequestParam String phone_number,
                           @RequestParam String city,
                           @RequestParam String country,
                           @RequestParam(required = false) MultipartFile file,
                           @RequestParam JobProfession profession){
        try {
            this.userService.register(email, password,
                    repeatedPassword, name, surname,phone_number,city,country,file,profession);
            return "redirect:/login";
        }catch (InvalidArgumentException | PasswordDoNotMatchException | EmailExistsException exception){
            return "redirect:/register?error="+exception.getMessage();
        }
    }
}
