package com.findyourservice.web.controller;

import com.findyourservice.config.EducationServiceTypeConverter;
import com.findyourservice.model.Educations;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.EducationServiceType;
import com.findyourservice.model.exceptions.EducationServiceNotFound;
import com.findyourservice.service.EducationsService;
import com.findyourservice.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/educations")
public class EducationsController {
    private final UserService userService;
    private final EducationsService educationsService;

    public EducationsController(UserService userService, EducationsService educationsService) {
        this.userService = userService;
        this.educationsService = educationsService;
    }


    @GetMapping
    public String educationsPage(@RequestParam(required = false) String error, Model model){
        if(error!=null && !error.isEmpty()){
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
        }

        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        model.addAttribute("bodyContent","educations");
        model.addAttribute("educations",this.educationsService.findAllByStatus(AdStatus.ACTIVE));
        return "master-template";
    }

    @GetMapping("/add")
    public String addEducationServicePage(Model model){
        User user = this.userService.getCurrentlyLoggedInUser();
        model.addAttribute("user",user);
        List<String> serviceTypes = Stream.of(EducationServiceType.values())
                .map(Enum::toString)
                .collect(Collectors.toList());

        model.addAttribute("serviceTypes",serviceTypes);
        model.addAttribute("bodyContent","add-education-service");
        return "master-template";
    }

    @GetMapping("/edit-form/{id}")
    public String editFoodServicePage(@PathVariable Long id, Model model){
        if(this.educationsService.findById(id).isPresent()){
            User user = this.userService.getCurrentlyLoggedInUser();
            model.addAttribute("user",user);

            Educations education = this.educationsService.findById(id).get();
            List<String> serviceTypes = Stream.of(EducationServiceType.values())
                    .map(Enum::toString)
                    .collect(Collectors.toList());

            model.addAttribute("serviceTypes",serviceTypes);
            model.addAttribute("service",education);
            model.addAttribute("bodyContent","add-education-service");
            return "master-template";
        }

        return "redirect:/educations?error=EducationServiceNotFoundError";
    }

    @PostMapping("/add")
    public String saveFoodService(RedirectAttributes redirectAttributes,
                                  @RequestParam(required = false) Long id,
                                  @RequestParam Long advertiserId,
                                  @RequestParam String advertiserName,
                                  @RequestParam String phoneNumber,
                                  @RequestParam String location,
                                  @RequestParam String subjects,
                                  @RequestParam String description,
                                  @RequestParam String price,
                                  @RequestParam String educationServiceType){
        if(educationServiceType.equals("PHYSICAL ATTENDANCE")){
            educationServiceType = "PHYSICAL_ATTENDANCE";
        }
        EducationServiceTypeConverter converter = new EducationServiceTypeConverter();
        EducationServiceType educationServiceTypeEnum = converter.convert(educationServiceType);
        if(id!=null){
            this.educationsService.editEducationService(id,advertiserId,advertiserName,phoneNumber,location,subjects,description,price,educationServiceTypeEnum);
        }else{
            this.educationsService.saveEducationService(advertiserId,advertiserName,phoneNumber,location,subjects,description,price,educationServiceTypeEnum);
            redirectAttributes.addFlashAttribute("message","Your service is being reviewed by administrator.");
        }
        return "redirect:/educations";

    }


    @GetMapping("/advertiser-service-info/{id}")
    public String advertiserServiceInfoPage(@PathVariable Long id,Model model){
        model.addAttribute("service",this.educationsService.findById(id).get());
        model.addAttribute("bodyContent","educations-advertiser-details");
        return "master-template";
    }
    @GetMapping("/customers-service-info/{id}")
    public String customersSerivceInfoPage(@PathVariable Long id,Model model){
        model.addAttribute("service",this.educationsService.findById(id).get());
        model.addAttribute("bodyContent","educations-customer-details");
        return "master-template";
    }

    @GetMapping("/add-client/{id}")
    public String addClientPage(@PathVariable Long id, Model model){


        User user = this.userService.getCurrentlyLoggedInUser();
        Educations education = this.educationsService.findById(id).get();
        if(user.getId().equals(education.getAdvertiserId())){
            model.addAttribute("bodyContent","add-education-client");
            model.addAttribute("allUsers",this.userService.findAll());
            model.addAttribute("educationServiceId",id);
        }else{
            model.addAttribute("bodyContent","educations");

        }
        return "master-template";

    }

    @PostMapping("/add-client")
    public String addClient(@RequestParam Long educationServiceId,
                            @RequestParam Long clientId){
        Educations education = this.educationsService.findById(educationServiceId).orElseThrow(()->new EducationServiceNotFound(educationServiceId));

        if(education.getEducationClients().stream().noneMatch(u-> u.getId().equals(clientId))){
            this.educationsService.addClient(educationServiceId,clientId);
            return "redirect:/educations";
        }else{
            return "redirect:/add-client/"+educationServiceId+"?error=UserAlreadyInThisService";
        }
    }

    @PostMapping("/finish-education-service/{id}")
    public String finishService(@PathVariable Long id){
        this.educationsService.finishService(id);
        return "redirect:/user/history-educations";
    }

    @PostMapping("/delete/{id}")
    @Transactional
    public String deleteCraftsmenService(@PathVariable Long id){
        User user = this.userService.getCurrentlyLoggedInUser();
        Educations education = this.educationsService.findById(id).get();
        if(user.getId().equals(education.getAdvertiserId()) ||
                user.getRole().toString().equals("ROLE_ADMIN")) {
            this.educationsService.deleteById(id);
        }
        return "redirect:/educations";
    }


    @PostMapping("/delete-client")
    public String deleteClient(@RequestParam Long id,
                               @RequestParam Long advertiserId,
                               @RequestParam Long clientId){

        User user = this.userService.getCurrentlyLoggedInUser();
        if(user.getId().equals(advertiserId)){
            this.educationsService.deleteClient(id, clientId);
        }

        return "redirect:/educations/advertiser-service-info/"+id;

    }

}
