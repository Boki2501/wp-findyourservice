package com.findyourservice.web.controller;

import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.ServiceType;
import com.findyourservice.service.RatingsService;
import com.findyourservice.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/ratings")
public class RatingsController {
    private final RatingsService ratingsService;
    private final UserService userService;
    public RatingsController(RatingsService ratingsService, UserService userService) {
        this.ratingsService = ratingsService;
        this.userService = userService;
    }

    @GetMapping("/{serviceId}&{userId}&{serviceType}")
    public String getRatingsPage(@PathVariable Long serviceId,
                                 @PathVariable Long userId,
                                 @PathVariable String serviceType, Model model){
        model.addAttribute("serviceId",serviceId);
        model.addAttribute("userId",userId);
        model.addAttribute("serviceType",serviceType);
        model.addAttribute("bodyContent","add-rating");
        return "master-template";
    }

    @PostMapping("/add-rating")
    public String addRating(@RequestParam(required = false) Long serviceId,
                            @RequestParam(required = false) Long userId,
                            @RequestParam(required = false) String serviceType,
                            @RequestParam(required = false) int rate,
                            @RequestParam(required = false) String comment){
        ServiceType ratingServiceType;
        String redirectLink;
        switch(serviceType){
            case "FOOD":
                ratingServiceType=ServiceType.FOOD;
                redirectLink="history-foods";
                break;
            case "CRAFTSMEN":
                ratingServiceType=ServiceType.CRAFTSMEN;
                redirectLink="history-craftsmen";
                break;
            case "EDUCATION":
                ratingServiceType=ServiceType.EDUCATION;
                redirectLink="history-educations";
                break;
            default:
                ratingServiceType=ServiceType.TRIP;
                redirectLink="history-trips";
                break;
        }
        User from_user = this.userService.getCurrentlyLoggedInUser();
        this.ratingsService.saveRating(from_user.getId(),userId,serviceId,ratingServiceType,rate,comment);

        return "redirect:/user/"+redirectLink;
    }
}
