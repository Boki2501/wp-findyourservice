package com.findyourservice.service;

import com.findyourservice.model.Food;
import com.findyourservice.model.enumerations.AdStatus;

import java.util.List;
import java.util.Optional;

public interface FoodService {

    List<Food> findAll();
    List<Food> findAllByStatus(AdStatus status);
    List<Food> findAllApprovalPendingFoodServices();
    Optional<Food> findById(Long id);
    Optional<Food> saveFoodService(Long advertiserId,
                                   String advertiserName,
                                   String phoneNumber,
                                   String location,
                                   String foodServiceType,
                                   String description,
                                   String price);
    Optional<Food> editFoodService(Long id,
                                   Long advertiserId,
                                   String advertiserName,
                                   String phoneNumber,
                                   String location,
                                   String foodServiceType,
                                   String description,
                                   String price);

    Optional<Food> addClient(Long serviceId,Long clientId);
    void deleteById(Long id);
    void deleteClient(Long id,Long clientId);
    void finishService(Long id);
    void approveService(Long id);

}
