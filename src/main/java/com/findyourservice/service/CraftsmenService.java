package com.findyourservice.service;

import com.findyourservice.model.Craftsmen;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.EducationServiceType;

import java.util.List;
import java.util.Optional;

public interface CraftsmenService {

    List<Craftsmen> findAll();
    List<Craftsmen> findAllByStatus(AdStatus status);
    List<Craftsmen> findAllApprovalPendingCraftsmenServices();
    Optional<Craftsmen> findById(Long id);
    Optional<Craftsmen> saveCraftsmenService(Long advertiserId,
                                             String advertiserName,
                                             String phoneNumber,
                                             String location,
                                             String profession,
                                             String description,
                                             String price);
    Optional<Craftsmen> editCraftsmenService(Long id,
                                              Long advertiserId,
                                              String advertiserName,
                                              String phoneNumber,
                                              String location,
                                              String profession,
                                              String description,
                                              String price);

    Optional<Craftsmen> addClient(Long serviceId,Long clientId);
    void deleteById(Long id);
    void deleteClient(Long id,Long clientId);
    void finishService(Long id);
    void approveService(Long id);
}
