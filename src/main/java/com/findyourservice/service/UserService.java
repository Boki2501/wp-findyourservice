package com.findyourservice.service;

import com.findyourservice.model.*;
import com.findyourservice.model.enumerations.AuthProvider;
import com.findyourservice.model.enumerations.JobProfession;
import com.findyourservice.model.enumerations.AdStatus;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {
    User register(String email, String password, String repeatPassword, String name,
                  String surname, String phone_number, String city,
                  String country, MultipartFile file, JobProfession profession);

    User editUser(Long id,String name,String surname,String phoneNumber,String city, String country, MultipartFile file);
    User getCurrentlyLoggedInUser();
    Optional<User> loadByEmail(String email);
    Optional<User> findById(Long id);
    void registerNewUserAfterOAuthLoginSuccess(String email, String name, AuthProvider provider);
    void updateExistingUserAfterOAuthLoginSuccess(User user, String name, AuthProvider provider);

    List<User> findAll();
    List<User> findAllApprovalPendingUsers();

    List<Trips> findAllUserTrips(Long id);
    List<Trips> findAllUserTripsByStatus(Long id, AdStatus adStatus);
    List<Food> findAllUserFoodServicesByStatus(Long id, AdStatus adStatus);
    List<Educations> findAllUserEducationServicesByStatus(Long id,AdStatus adStatus);
    List<Craftsmen> findAllUserCraftsmenServicesByStatus(Long id, AdStatus adStatus);
    void deleteById(Long id);
    void changePassword(Long id,String email,String oldPassword,String newPassword,String repeatedPassword);
    void changeProfession(Long id,JobProfession jobProfession);
    void approveUser(Long id);
    void giveRoleToUser(Long id);


}
