package com.findyourservice.service;

import com.findyourservice.model.RateComments;
import com.findyourservice.model.Ratings;
import com.findyourservice.model.enumerations.ServiceType;

import java.util.List;
import java.util.Optional;

public interface RatingsService {

    Optional<Ratings> saveRating(Long fromUserId,
                                 Long toUserId,
                                 Long tripId,
                                 ServiceType serviceType,
                                 int rate,
                                 String comment);

    List<Long> findAllServicesRatedFromUserId(Long id, ServiceType serviceType);

    List<String> findAllCommentsGivenToUser(Long id);
    List<RateComments> findAllRatesAndCommentGivenToUser(Long id);

}
