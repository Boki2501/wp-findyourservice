package com.findyourservice.service.impl;

import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AuthProvider;
import com.findyourservice.model.exceptions.OAuth2AuthenticationProcessingException;
import com.findyourservice.repository.jpa.UserRepository;
//import com.findyourservice.security.UserPrincipal;
//import com.findyourservice.security.oauth2.user.OAuth2UserInfo;
//import com.findyourservice.security.oauth2.user.OAuth2UserInfoFactory;
import com.findyourservice.security.oauth2.user.CustomOAuth2User;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {


    private final UserRepository userRepository;

    public CustomOAuth2UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);
        return new CustomOAuth2User(oAuth2User,oAuth2UserRequest.getClientRegistration().getRegistrationId());
    }
}
