package com.findyourservice.service.impl;

import com.findyourservice.model.RateComments;
import com.findyourservice.model.Ratings;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.ServiceType;
import com.findyourservice.repository.jpa.RatingsRepository;
import com.findyourservice.repository.jpa.UserRepository;
import com.findyourservice.service.RatingsService;
import org.springframework.stereotype.Service;


import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

@Service
public class RatingsServiceImpl implements RatingsService {
    private final RatingsRepository ratingsRepository;
    private final UserRepository userRepository;
    public RatingsServiceImpl(RatingsRepository ratingsRepository, UserRepository userRepository) {
        this.ratingsRepository = ratingsRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Optional<Ratings> saveRating(Long fromUserId,
                                        Long toUserId, Long serviceId,
                                        ServiceType serviceType, int rate, String comment) {

        Ratings rating = new Ratings(fromUserId,toUserId,serviceId,serviceType,rate,comment);
        User user = this.userRepository.findUserById(toUserId).get();

        List<Integer> userRatings = this.ratingsRepository.findAllRatingsGivenToUserId(toUserId);
        userRatings.add(rate);
        double user_rate = userRatings.stream().mapToDouble(a->a).average().orElse(0.0);
        user.setRating(user_rate);
        this.userRepository.save(user);

        return Optional.of(this.ratingsRepository.save(rating));
    }

    @Override
    public List<Long> findAllServicesRatedFromUserId(Long id, ServiceType serviceType) {
        return this.ratingsRepository.findAllServicesRatedFromUserId(id,serviceType);
    }

    @Override
    public List<String> findAllCommentsGivenToUser(Long id) {
        return this.ratingsRepository.findAllCommentsGivenToUser(id);
    }

    @Override
    public List<RateComments> findAllRatesAndCommentGivenToUser(Long id) {
        return this.ratingsRepository.findAllRatesAndCommentGivenToUser(id);
    }
}
