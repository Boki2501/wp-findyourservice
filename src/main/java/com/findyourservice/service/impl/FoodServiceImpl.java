package com.findyourservice.service.impl;

import com.findyourservice.model.Food;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.exceptions.FoodServiceNotFound;
import com.findyourservice.model.exceptions.UserNotFoundException;
import com.findyourservice.repository.jpa.FoodRepository;
import com.findyourservice.repository.jpa.UserRepository;
import com.findyourservice.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FoodServiceImpl implements FoodService {
    private final FoodRepository foodRepository;
    private final UserRepository userRepository;

    @Autowired
    private JavaMailSender emailSender;

    public FoodServiceImpl(FoodRepository foodRepository, UserRepository userRepository) {
        this.foodRepository = foodRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Food> findAll() {
        return this.foodRepository.findAll().stream().filter(Food::isEnabled).collect(Collectors.toList());
    }

    @Override
    public List<Food> findAllByStatus(AdStatus status) {

        return this.foodRepository.findAllByStatus(status)
                .stream()
                .filter(Food::isEnabled)
                .collect(Collectors.toList());
    }

    @Override
    public List<Food> findAllApprovalPendingFoodServices() {
        return this.foodRepository.findAll().stream().filter(f->!f.isEnabled()).collect(Collectors.toList());
    }

    @Override
    public Optional<Food> findById(Long id) {
        return this.foodRepository.findById(id);
    }

    @Override
    public Optional<Food> saveFoodService(Long advertiserId, String advertiserName, String phoneNumber, String location, String foodServiceType, String description, String price) {
        Food food = new Food(advertiserId,advertiserName,phoneNumber,location,foodServiceType,description,price);
        User user = this.userRepository.findUserById(advertiserId).get();
        food.getFoodClients().add(user);


        return Optional.of(this.foodRepository.save(food));
    }

    @Override
    public Optional<Food> editFoodService(Long id, Long advertiserId, String advertiserName, String phoneNumber, String location, String foodServiceType, String description, String price) {
        Food food = this.foodRepository.findById(id).orElseThrow(()->new FoodServiceNotFound(id));
        food.setAdvertiserName(advertiserName);
        food.setPhoneNumber(phoneNumber);
        food.setLocation(location);
        food.setFoodServiceType(foodServiceType);
        food.setDescription(description);
        food.setPrice(price);


        return Optional.of(this.foodRepository.save(food));
    }

    @Override
    public Optional<Food> addClient(Long serviceId, Long clientId) {
        Food food = this.foodRepository.findById(serviceId).orElseThrow(()->new FoodServiceNotFound(serviceId));
        User user = this.userRepository.findUserById(clientId).orElseThrow(()->new UserNotFoundException(clientId));

        food.getFoodClients().add(user);
        this.foodRepository.save(food);
        user.getFoodServices().add(food);
        this.userRepository.save(user);

        return Optional.of(food);
    }

    @Override
    public void deleteById(Long id) {
        this.foodRepository.deleteById(id);
    }

    @Override
    public void deleteClient(Long id, Long clientId) {
        Food food = this.foodRepository.findById(id).orElseThrow(()->new FoodServiceNotFound(id));
        food.getFoodClients().removeIf(u-> u.getId().equals(clientId));
        this.foodRepository.save(food);
    }

    @Override
    public void finishService(Long id) {
        Food food = this.foodRepository.findById(id).orElseThrow(()-> new FoodServiceNotFound(id));
        food.setStatus(AdStatus.FINISHED);
        this.foodRepository.save(food);
    }

    @Override
    public void approveService(Long id) {
        Food food = this.foodRepository.findById(id).orElseThrow(()->new FoodServiceNotFound(id));
        food.setEnabled(true);
        User user = this.userRepository.findUserById(food.getAdvertiserId())
                .orElseThrow(()->new UserNotFoundException(food.getAdvertiserId()));
        sendSimpleMessage(user.getEmail(),"Service approved!",
                "Your service has been approved successfully!");
        this.foodRepository.save(food);
    }

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("borcestanoev@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }
}
