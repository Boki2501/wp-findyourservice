package com.findyourservice.service.impl;

import com.findyourservice.model.Trips;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.exceptions.FreeSlotsException;
import com.findyourservice.model.exceptions.TripNotFoundException;
import com.findyourservice.model.exceptions.UserNotFoundException;
import com.findyourservice.repository.jpa.TripsRepository;
import com.findyourservice.repository.jpa.UserRepository;
import com.findyourservice.service.TripsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class
TripsServiceImpl implements TripsService {
    private final TripsRepository tripsRepository;
    private final UserRepository userRepository;
    @Autowired
    private JavaMailSender emailSender;

    public TripsServiceImpl(TripsRepository tripsRepository, UserRepository userRepository) {
        this.tripsRepository = tripsRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Trips> findAll() {
        return tripsRepository.findAll().stream().filter(Trips::isEnabled).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<Trips> findAllByStatus(AdStatus adStatus) {
        return this.tripsRepository.findAllByStatus(adStatus)
                .stream()
                .filter(Trips::isEnabled)
                .collect(Collectors.toList());
    }

    @Override
    public List<Trips> findAllApprovalPendingTrips() {
        return this.tripsRepository.findAll()
                .stream()
                .filter(t->!t.isEnabled())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Trips> findById(Long id) {
        return this.tripsRepository.findById(id);
    }


    @Override
    @Transactional
    public Optional<Trips> saveTrip(Long driverId, String driverName, String phoneNumber, String fromLocation, String toLocation, String description, int price, int freeSlots, String departureDate, String departureTime) {

        Trips trip = new Trips(driverId, driverName, phoneNumber, fromLocation, toLocation, description, price, freeSlots,departureDate, departureTime);

        User user = this.userRepository.findUserById(driverId).get();
        trip.getPassengers().add(user);
//        user.getTrips().add(trip);
//        this.userRepository.save(user);

        return Optional.of(this.tripsRepository.save(trip));
    }

    @Override
    public Optional<Trips> editTrip(Long id,Long driverId,
                                    String driverName,
                                    String phoneNumber,
                                    String fromLocation,
                                    String toLocation,
                                    String description,
                                    int price,
                                    int freeSlots,
                                    String departureDate,
                                    String departureTime) {

        Trips trip = this.tripsRepository.findById(id).orElseThrow(()->new TripNotFoundException(id));
        trip.setDriverName(driverName);
        trip.setPhoneNumber(phoneNumber);
        trip.setFromLocation(fromLocation);
        trip.setToLocation(toLocation);
        trip.setDescription(description);
        trip.setPrice(price);
        trip.setFreeSlots(freeSlots);
        trip.setDepartureDate(departureDate);
        trip.setDepartureTime(departureTime);

        return Optional.of(this.tripsRepository.save(trip));
    }

    @Override
    public Optional<Trips> addPassenger(Long tripId, Long passengerId) {
        Trips trip = this.tripsRepository.findById(tripId).orElseThrow(()->new TripNotFoundException(tripId));
        User user = this.userRepository.findUserById(passengerId).orElseThrow(()->new UserNotFoundException(passengerId));

        int freeslots = trip.getFreeSlots()-1;
        if(freeslots<0) throw new FreeSlotsException();

        trip.getPassengers().add(user);
        trip.setFreeSlots(freeslots);
        this.tripsRepository.save(trip);
        user.getTrips().add(trip);
        this.userRepository.save(user);

        return Optional.of(trip);

    }

    @Override
    public void deleteById(Long id) {
        this.tripsRepository.deleteById(id);
    }

    @Override
    public void deletePassenger(Long tripId, Long passengerId) {
        Trips trip = this.tripsRepository.findById(tripId).orElseThrow(()->new TripNotFoundException(tripId));
        trip.getPassengers().removeIf(u-> u.getId().equals(passengerId));
        this.tripsRepository.save(trip);
    }

    @Override
    public void finishTrip(Long tripId) {
        Trips trip = this.tripsRepository.findById(tripId).get();
        trip.setStatus(AdStatus.FINISHED);
        this.tripsRepository.save(trip);
    }

    @Override
    public void approveTrip(Long id) {
        Trips trip = this.tripsRepository.findById(id).orElseThrow(()->new TripNotFoundException(id));
        User user = this.userRepository.findUserById(trip.getDriverId())
                .orElseThrow(()->new UserNotFoundException(trip.getDriverId()));
        trip.setEnabled(true);
        sendSimpleMessage(user.getEmail(),"Service approved!",
                "Your service has been approved successfully!");
        this.tripsRepository.save(trip);
    }



    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("borcestanoev@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }
}
