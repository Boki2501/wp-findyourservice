package com.findyourservice.service.impl;

import com.findyourservice.model.User;
import com.findyourservice.model.exceptions.AccountNotEnabledException;
import com.findyourservice.model.exceptions.InvalidArgumentException;
import com.findyourservice.model.exceptions.InvalidUserCredentialsException;
import com.findyourservice.repository.jpa.UserRepository;
import com.findyourservice.service.AuthService;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    public AuthServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User login(String username, String password) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            throw  new InvalidArgumentException();
        }
        User user =  userRepository.findByUsernameAndPassword(username,password)
                .orElseThrow(InvalidUserCredentialsException::new);

        if(!user.isEnabled()) throw new AccountNotEnabledException();

        return user;
    }
}
