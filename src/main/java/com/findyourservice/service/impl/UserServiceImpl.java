package com.findyourservice.service.impl;

import com.findyourservice.model.*;
import com.findyourservice.model.enumerations.AuthProvider;
import com.findyourservice.model.enumerations.JobProfession;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.Role;
import com.findyourservice.model.exceptions.*;
import com.findyourservice.repository.jpa.UserRepository;
import com.findyourservice.security.oauth2.user.CustomOAuth2User;
import com.findyourservice.service.UserService;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
@PersistenceContext private EntityManager em;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Autowired
    private JavaMailSender emailSender;
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User register(String email, String password, String repeatPassword, String name, String surname, String phone_number, String city, String country, MultipartFile file, JobProfession profession) {
        if(email==null || email.isEmpty() || password==null || password.isEmpty()){
            throw  new InvalidUsernameOrPasswordException();
        }
        if(!password.equals(repeatPassword))
            throw new PasswordDoNotMatchException();

        if(this.userRepository.findByEmail(email).isPresent()){
            throw new EmailExistsException(email);
        }
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String profileImg = "";

        if(fileName.isEmpty()){
            try {
                profileImg = Base64.getEncoder().encodeToString(Files.readAllBytes(Path.of("D:\\IntelliJProjects\\findyourservice-project\\src\\main\\resources\\static\\images\\default_profile_image.png")));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else {
            if (fileName.contains("..")) {
                throw new InvalidFileFound();
            }
            try {
                profileImg = Base64.getEncoder().encodeToString(file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        User user = new User(email, passwordEncoder.encode(password), name, surname, phone_number, city, country, profileImg, profession);
        userRepository.save(user);
        return user;
    }

    @Override
    public User editUser(Long id, String name, String surname, String phoneNumber, String city, String country, MultipartFile file) {
        User user = this.userRepository.findUserById(id).orElseThrow(()-> new UserNotFoundException(id));
        user.setName(name);
        user.setSurname(surname);
        user.setPhone_number(phoneNumber);
        user.setCity(city);
        user.setCountry(country);

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String profileImg ="";
       if(!fileName.isEmpty()) {
           try {
               profileImg = Base64.getEncoder().encodeToString(file.getBytes());
               user.setProfileImg(profileImg);

               em.clear();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }

        this.userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    public User getCurrentlyLoggedInUser() {
        Object authentication = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user;
        if(authentication instanceof CustomOAuth2User){
            user = this.loadByEmail(((CustomOAuth2User) authentication).getEmail()).get();
        }else if(authentication instanceof User){
            user = this.loadByEmail(((User) authentication).getEmail()).get();
        }else{
            return null;
        }
        return user;
    }

    @Override
    public Optional<User> loadByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> findById(Long id) {
        return this.userRepository.findUserById(id);
    }

    @Override
    public void registerNewUserAfterOAuthLoginSuccess(String email, String name, AuthProvider provider) {
        User user = new User();
        user.setEmail(email);
        String[] name_split = name.split(" ");
        user.setName(name_split[0]);
        user.setSurname(name_split[1]);
        user.setProvider(provider);
        user.setUsername(name);
        user.setEnabled(true);
        String profileImg="";
        try {
            profileImg = Base64.getEncoder().encodeToString(Files.readAllBytes(Path.of("D:\\IntelliJProjects\\findyourservice-project\\src\\main\\resources\\static\\images\\default_profile_image.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        user.setProfileImg(profileImg);

        userRepository.save(user);
    }

    @Override
    public void updateExistingUserAfterOAuthLoginSuccess(User user, String name, AuthProvider provider) {
        user.setName(name);
        String[] name_split = name.split(" ");
        user.setName(name_split[0]);
        user.setSurname(name_split[1]);
        user.setUsername(name);
        String profileImg="";
        if(user.getProfileImg()==null){
            try {
                profileImg = Base64.getEncoder().encodeToString(Files.readAllBytes(Path.of("D:\\IntelliJProjects\\findyourservice-project\\src\\main\\resources\\static\\images\\default_profile_image.png")));
            } catch (IOException e) {
                e.printStackTrace();
            }
            user.setProfileImg(profileImg);
        }
        userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public List<User> findAllApprovalPendingUsers() {
        return this.userRepository.findAll().stream().filter(u-> !u.isEnabled()).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<Trips> findAllUserTrips(Long id) {
        return this.userRepository.findAllTripsByUserId(id);
    }

    @Override
    @Transactional
    public List<Trips> findAllUserTripsByStatus(Long id, AdStatus adStatus) {
        List<Trips> trips = this.userRepository.findAllTripsByUserId(id);
        return trips.stream().filter(t->t.getStatus() == adStatus).collect(Collectors.toList());
    }

    @Override
    public List<Food> findAllUserFoodServicesByStatus(Long id, AdStatus adStatus) {
        List<Food> foodServices = this.userRepository.findAllFoodServicesByUserId(id);
        return foodServices.stream().filter(f-> f.getStatus() == adStatus).collect(Collectors.toList());
    }

    @Override
    public List<Educations> findAllUserEducationServicesByStatus(Long id, AdStatus adStatus) {
        List<Educations> educationServices = this.userRepository.findAllEducationServicesByUserId(id);
        return educationServices.stream().filter(e->e.getStatus()==adStatus).collect(Collectors.toList());
    }

    @Override
    public List<Craftsmen> findAllUserCraftsmenServicesByStatus(Long id, AdStatus adStatus) {
        List<Craftsmen> craftsmenServices = this.userRepository.findAllCraftsmenServicesByUserId(id);
        return craftsmenServices.stream().filter(c->c.getStatus()==adStatus).collect(Collectors.toList());
    }


    @Override
    @Transactional
    public void deleteById(Long id) {
        this.userRepository.deleteUserById(id);
    }

    @Override
    @Transactional
    public void changePassword(Long id,String email,String oldPassword, String newPassword, String repeatedPassword) {
        UserDetails user = this.loadUserByUsername(email);
        User db_user = this.userRepository.findUserById(id).orElseThrow(()->new UserNotFoundException(id));
        if(!this.passwordEncoder.matches(oldPassword,user.getPassword())){
            throw new BadCredentialsException("Old password do not match");
        }

        if(!newPassword.equals(repeatedPassword))
            throw new PasswordDoNotMatchException();

        db_user.setPassword(this.passwordEncoder.encode(newPassword));
        this.userRepository.save(db_user);
    }

    @Override
    public void changeProfession(Long id, JobProfession jobProfession) {
        User user = this.userRepository.findUserById(id).orElseThrow(()->new UserNotFoundException(id));
        user.setProfession(jobProfession);
        this.userRepository.save(user);
    }

    @Override
    @Transactional
    public void approveUser(Long id){
        User user = this.userRepository.findUserById(id).orElseThrow(()->new UserNotFoundException(id));
        user.setEnabled(true);
        sendSimpleMessage(user.getEmail(),"User account approved!",
                "Your account has been approved successfully!");
        this.userRepository.save(user);
    }

    @Override
    public void giveRoleToUser(Long id) {
        User user = this.userRepository.findUserById(id).orElseThrow(()->new UserNotFoundException(id));
        user.setRole(Role.ROLE_ADMIN);
        this.userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return this.userRepository.findByEmail(email).orElseThrow(()-> new BadCredentialsException("Email does not exist"));
    }

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("borcestanoev@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

}
