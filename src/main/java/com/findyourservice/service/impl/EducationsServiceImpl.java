package com.findyourservice.service.impl;

import com.findyourservice.model.Educations;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.EducationServiceType;
import com.findyourservice.model.exceptions.EducationServiceNotFound;
import com.findyourservice.model.exceptions.UserNotFoundException;
import com.findyourservice.repository.jpa.EducationsRepository;
import com.findyourservice.repository.jpa.UserRepository;
import com.findyourservice.service.EducationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EducationsServiceImpl implements EducationsService {
    private final EducationsRepository educationsRepository;
    private final UserRepository userRepository;

    @Autowired
    private JavaMailSender emailSender;

    public EducationsServiceImpl(EducationsRepository educationsRepository, UserRepository userRepository) {
        this.educationsRepository = educationsRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Educations> findAll() {
        return this.educationsRepository.findAll().stream().filter(Educations::isEnabled).collect(Collectors.toList());
    }

    @Override
    public List<Educations> findAllByStatus(AdStatus status) {
        return this.educationsRepository.findAllByStatus(status)
                .stream()
                .filter(Educations::isEnabled)
                .collect(Collectors.toList());
    }

    @Override
    public List<Educations> findAllApprovalPendingEducationServices() {
        return this.educationsRepository.findAll().stream().filter(e->!e.isEnabled()).collect(Collectors.toList());
    }

    @Override
    public Optional<Educations> findById(Long id) {
        return this.educationsRepository.findById(id);
    }

    @Override
    public Optional<Educations> saveEducationService(Long advertiserId, String advertiserName, String phoneNumber, String location, String subjects, String description, String price, EducationServiceType educationServiceType) {
        Educations education = new Educations(advertiserId,advertiserName,phoneNumber,location,subjects,description,price,educationServiceType);
        User user = this.userRepository.findUserById(advertiserId).orElseThrow(()->new UserNotFoundException(advertiserId));
        education.getEducationClients().add(user);

        return Optional.of(this.educationsRepository.save(education));
    }

    @Override
    public Optional<Educations> editEducationService(Long id, Long advertiserId, String advertiserName, String phoneNumber, String location, String subjects, String description, String price, EducationServiceType educationServiceType) {
        Educations education = this.educationsRepository.findById(id).orElseThrow(()->new EducationServiceNotFound(id));
        education.setAdvertiserName(advertiserName);
        education.setPhoneNumber(phoneNumber);
        education.setLocation(location);
        education.setSubjects(subjects);
        education.setDescription(description);
        education.setPrice(price);
        education.setEducationServiceType(educationServiceType);

        return Optional.of(this.educationsRepository.save(education));
    }

    @Override
    public Optional<Educations> addClient(Long serviceId, Long clientId) {
        Educations education = this.educationsRepository.findById(serviceId).orElseThrow(()->new EducationServiceNotFound(serviceId));
        User user = this.userRepository.findUserById(clientId).orElseThrow(()->new UserNotFoundException(clientId));

        education.getEducationClients().add(user);
        this.educationsRepository.save(education);

        user.getEducationServices().add(education);
        this.userRepository.save(user);

        return Optional.of(education);
    }

    @Override
    public void deleteById(Long id) {
        this.educationsRepository.deleteById(id);
    }

    @Override
    public void deleteClient(Long id, Long clientId) {
        Educations education = this.educationsRepository.findById(id).orElseThrow(()->new EducationServiceNotFound(id));
        education.getEducationClients().removeIf(u-> u.getId().equals(clientId));
        this.educationsRepository.save(education);
    }

    @Override
    public void finishService(Long id) {
        Educations education = this.educationsRepository.findById(id).orElseThrow(()->new EducationServiceNotFound(id));
        education.setStatus(AdStatus.FINISHED);
        this.educationsRepository.save(education);
    }

    @Override
    public void approveService(Long id) {
        Educations education = this.educationsRepository.findById(id).orElseThrow(()->new EducationServiceNotFound(id));
        education.setEnabled(true);

        User user = this.userRepository.findUserById(education.getAdvertiserId())
                .orElseThrow(()->new UserNotFoundException(education.getAdvertiserId()));
        sendSimpleMessage(user.getEmail(),"Service approved!",
                "Your service has been approved successfully!");

        this.educationsRepository.save(education);
    }

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("borcestanoev@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }
}
