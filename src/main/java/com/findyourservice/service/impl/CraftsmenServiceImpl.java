package com.findyourservice.service.impl;

import com.findyourservice.model.Craftsmen;
import com.findyourservice.model.Educations;
import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.exceptions.CraftsmenServiceNotFound;
import com.findyourservice.model.exceptions.EducationServiceNotFound;
import com.findyourservice.model.exceptions.UserNotFoundException;
import com.findyourservice.repository.jpa.CraftsmenRepository;
import com.findyourservice.repository.jpa.UserRepository;
import com.findyourservice.service.CraftsmenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CraftsmenServiceImpl implements CraftsmenService {
    private final CraftsmenRepository craftsmenRepository;
    private final UserRepository userRepository;

    @Autowired
    private JavaMailSender emailSender;

    public CraftsmenServiceImpl(CraftsmenRepository craftsmenRepository, UserRepository userRepository) {
        this.craftsmenRepository = craftsmenRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Craftsmen> findAll() {
        return this.craftsmenRepository.findAll().stream().filter(Craftsmen::isEnabled).collect(Collectors.toList());
    }

    @Override
    public List<Craftsmen> findAllByStatus(AdStatus status) {
        return this.craftsmenRepository.findAllByStatus(status)
                .stream()
                .filter(Craftsmen::isEnabled)
                .collect(Collectors.toList());
    }

    @Override
    public List<Craftsmen> findAllApprovalPendingCraftsmenServices() {
        return this.craftsmenRepository.findAll().stream().filter(c->!c.isEnabled()).collect(Collectors.toList());
    }

    @Override
    public Optional<Craftsmen> findById(Long id) {
        return this.craftsmenRepository.findById(id);
    }

    @Override
    public Optional<Craftsmen> saveCraftsmenService(Long advertiserId, String advertiserName, String phoneNumber, String location, String profession, String description, String price) {
       Craftsmen craftsmen = new Craftsmen(advertiserId,advertiserName,phoneNumber,location,profession,description,price);
       User user = this.userRepository.findUserById(advertiserId).orElseThrow(()->new UserNotFoundException(advertiserId));
       craftsmen.getCraftsmenServiceClients().add(user);



       return Optional.of(this.craftsmenRepository.save(craftsmen));
    }

    @Override
    public Optional<Craftsmen> editCraftsmenService(Long id, Long advertiserId, String advertiserName, String phoneNumber, String location, String profession, String description, String price) {
        Craftsmen craftsmen = this.craftsmenRepository.findById(id).orElseThrow(()->new CraftsmenServiceNotFound(id));
        craftsmen.setAdvertiserName(advertiserName);
        craftsmen.setPhoneNumber(phoneNumber);
        craftsmen.setLocation(location);
        craftsmen.setProfession(profession);
        craftsmen.setDescription(description);
        craftsmen.setPrice(price);


        return Optional.of(this.craftsmenRepository.save(craftsmen));
    }

    @Override
    public Optional<Craftsmen> addClient(Long serviceId, Long clientId) {
        Craftsmen craftsmen = this.craftsmenRepository.findById(serviceId).orElseThrow(()->new CraftsmenServiceNotFound(serviceId));
        User user = this.userRepository.findUserById(clientId).orElseThrow(()->new UserNotFoundException(clientId));

        craftsmen.getCraftsmenServiceClients().add(user);
        this.craftsmenRepository.save(craftsmen);

        user.getCraftsmenServices().add(craftsmen);
        this.userRepository.save(user);

        return Optional.of(craftsmen);

    }

    @Override
    public void deleteById(Long id) {
        this.craftsmenRepository.deleteById(id);
    }

    @Override
    public void deleteClient(Long id, Long clientId) {
        Craftsmen craftsmen = this.craftsmenRepository.findById(id).orElseThrow(()->new CraftsmenServiceNotFound(id));
        craftsmen.getCraftsmenServiceClients().removeIf(u->u.getId().equals(clientId));
        this.craftsmenRepository.save(craftsmen);
    }

    @Override
    public void finishService(Long id) {
        Craftsmen craftsmen = this.craftsmenRepository.findById(id).orElseThrow(()->new CraftsmenServiceNotFound(id));
        craftsmen.setStatus(AdStatus.FINISHED);
        this.craftsmenRepository.save(craftsmen);
    }

    @Override
    public void approveService(Long id) {
        Craftsmen craftsmen = this.craftsmenRepository.findById(id).orElseThrow(()->new CraftsmenServiceNotFound(id));
        craftsmen.setEnabled(true);
        User user = this.userRepository.findUserById(craftsmen.getAdvertiserId())
                .orElseThrow(()->new UserNotFoundException(craftsmen.getAdvertiserId()));
        sendSimpleMessage(user.getEmail(),"Service approved!",
                "Your service has been approved successfully!");

        this.craftsmenRepository.save(craftsmen);
    }

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("borcestanoev@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }
}
