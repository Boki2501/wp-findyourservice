package com.findyourservice.service;

import com.findyourservice.model.User;

public interface AuthService {

    User login(String username, String password);

}
