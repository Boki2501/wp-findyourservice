package com.findyourservice.service;

import com.findyourservice.model.Educations;
import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.EducationServiceType;

import java.util.List;
import java.util.Optional;

public interface EducationsService {
    List<Educations> findAll();
    List<Educations> findAllByStatus(AdStatus status);
    List<Educations> findAllApprovalPendingEducationServices();
    Optional<Educations> findById(Long id);
    Optional<Educations> saveEducationService(Long advertiserId,
                                         String advertiserName,
                                         String phoneNumber,
                                         String location,
                                         String subjects,
                                         String description,
                                         String price,
                                         EducationServiceType educationServiceType);
    Optional<Educations> editEducationService(Long id,
                                              Long advertiserId,
                                              String advertiserName,
                                              String phoneNumber,
                                              String location,
                                              String subjects,
                                              String description,
                                              String price,
                                              EducationServiceType educationServiceType);

    Optional<Educations> addClient(Long serviceId,Long clientId);
    void deleteById(Long id);
    void deleteClient(Long id,Long clientId);
    void finishService(Long id);
    void approveService(Long id);
}
