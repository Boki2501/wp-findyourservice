package com.findyourservice.service;

import com.findyourservice.model.Trips;
import com.findyourservice.model.enumerations.AdStatus;

import java.util.List;
import java.util.Optional;

public interface TripsService {

    List<Trips> findAll();
    List<Trips> findAllByStatus(AdStatus adStatus);
    List<Trips> findAllApprovalPendingTrips();
    Optional<Trips> findById(Long id);
    Optional<Trips> saveTrip(Long driverId,
                             String driverName,
                             String phoneNumber,
                             String fromLocation,
                             String toLocation,
                             String description,
                             int price,
                             int freeSlots,
                             String departureDate,
                             String departureTime);

    Optional<Trips> editTrip(Long id,
                             Long driverId,
                             String driverName,
                             String phoneNumber,
                             String fromLocation,
                             String toLocation,
                             String description,
                             int price,
                             int freeSlots,
                             String departureDate,
                             String departureTime);

    Optional<Trips> addPassenger(Long tripId, Long passengerId);

    void deleteById(Long id);
    void deletePassenger(Long tripId,Long passengerId);
    void finishTrip(Long tripId);
    void approveTrip(Long id);
}
