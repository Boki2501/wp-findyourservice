package com.findyourservice.model;

import com.findyourservice.model.enumerations.AdStatus;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long advertiserId;
    private String advertiserName;
    private String phoneNumber;

    private String location;
    private String foodServiceType;
    @Column(length = 300)
    private String description;
    private String price;

    //    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String dateCreated;
    private boolean isEnabled;

    @Enumerated(EnumType.STRING)
    private AdStatus status;

    @ManyToMany(cascade = {CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_food",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "food_id"))
    private List<User> foodClients;

    public Food(){}

    public Food(Long advertiserId, String advertiserName, String phoneNumber, String location,String foodServiceType, String description, String price) {
        this.advertiserId = advertiserId;
        this.advertiserName = advertiserName;
        this.phoneNumber = phoneNumber;
        this.location = location;
        this.foodServiceType = foodServiceType;
        this.description = description;
        this.price = price;

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        this.dateCreated = myDateObj.format(myFormatObj);
        this.status = AdStatus.ACTIVE;
        this.foodClients = new ArrayList<>();
        this.isEnabled = false;
    }
}
