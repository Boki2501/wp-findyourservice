package com.findyourservice.model;

import com.findyourservice.model.enumerations.AdStatus;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Trips {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long driverId;
    private String driverName;
    private String phoneNumber;

    private String toLocation;
    private String fromLocation;
    private String description;
    private int price;
    private int freeSlots;

//    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String departureDate;
    private String departureTime;

    private boolean isEnabled;

    @Enumerated(EnumType.STRING)
    private AdStatus status;

    @ManyToMany(cascade = {CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_trip",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "trip_id"))
    private List<User> passengers;


    public Trips(){}

    public Trips(Long driverId, String driverName,
                 String phoneNumber, String fromLocation,
                 String toLocation, String description,
                 int price, int freeSlots, String departureDate,
                 String departureTime) {
        this.driverId = driverId;
        this.driverName = driverName;
        this.phoneNumber = phoneNumber;
        this.toLocation = toLocation;
        this.fromLocation = fromLocation;
        this.description = description;
        this.price = price;
        this.freeSlots = freeSlots;
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.status = AdStatus.ACTIVE;
        this.passengers = new ArrayList<>();
        this.isEnabled=false;
    }
}
