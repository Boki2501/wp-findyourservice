package com.findyourservice.model.enumerations;

public enum AdStatus {
    ACTIVE,
    FINISHED
}
