package com.findyourservice.model.enumerations;

public enum EducationServiceType {
    ONLINE("ONLINE"),
    PHYSICAL_ATTENDANCE("PHYSICAL ATTENDANCE"),
    COMBINED("COMBINED");



    private String displayName;

    EducationServiceType(String displayName){
        this.displayName =displayName;
    }




    public String displayName() { return displayName; }

    // Optionally and/or additionally, toString.
    @Override public String toString() { return displayName; }
}
