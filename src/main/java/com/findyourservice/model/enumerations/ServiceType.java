package com.findyourservice.model.enumerations;

public enum ServiceType {
    TRIP,
    FOOD,
    EDUCATION,
    CRAFTSMEN
}
