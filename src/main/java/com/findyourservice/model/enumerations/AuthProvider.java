package com.findyourservice.model.enumerations;

public enum  AuthProvider {
    local,
    facebook,
    google
}