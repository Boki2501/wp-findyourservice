package com.findyourservice.model.enumerations;

public enum JobProfession {
    DRIVER,
    CRAFTSMAN,
    CHEF,
    WAITER,
    PROFESSOR,
    CUSTOMER

}
