package com.findyourservice.model.enumerations;

public enum ServicePriceType {
    ON_DEMAND("ON DEMAND"),
    BY_PERSON("BY PERSON"),
    BY_HOUR("BY HOUR");

    private String displayName;

    ServicePriceType(String displayName){
        this.displayName =displayName;
    }




    public String displayName() { return displayName; }

    // Optionally and/or additionally, toString.
    @Override public String toString() { return displayName; }
}
