package com.findyourservice.model;

import com.findyourservice.model.enumerations.AdStatus;
import com.findyourservice.model.enumerations.EducationServiceType;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Educations {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long advertiserId;
    private String advertiserName;
    private String phoneNumber;

    private String location;
    private String subjects;
    private String description;
    private String price;

//        @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String dateCreated;
    private boolean isEnabled;

    @Enumerated(EnumType.STRING)
    private AdStatus status;

    @Enumerated(EnumType.STRING)
    private EducationServiceType educationServiceType;

    @ManyToMany(cascade = {CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_education",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "education_id"))
    private List<User> educationClients;


    public Educations() {}

    public Educations(Long advertiserId, String advertiserName, String phoneNumber, String location, String subjects, String description, String price, EducationServiceType educationServiceType) {
        this.advertiserId = advertiserId;
        this.advertiserName = advertiserName;
        this.phoneNumber = phoneNumber;
        this.location = location;
        this.subjects = subjects;
        this.description = description;
        this.price = price;
        this.status = AdStatus.ACTIVE;
        this.educationServiceType = educationServiceType;

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        this.dateCreated = myDateObj.format(myFormatObj);
        this.educationClients = new ArrayList<>();
        this.isEnabled = false;
    }
}
