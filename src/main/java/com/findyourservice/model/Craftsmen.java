package com.findyourservice.model;

import com.findyourservice.model.enumerations.AdStatus;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Craftsmen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long advertiserId;
    private String advertiserName;
    private String phoneNumber;

    private String location;
    private String profession;
    private String description;
    private String price;

    //        @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String dateCreated;
    private boolean isEnabled;


    @Enumerated(EnumType.STRING)
    private AdStatus status;

    @ManyToMany(cascade = {CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_craftsmen",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private List<User> craftsmenServiceClients;


    public Craftsmen() {}

    public Craftsmen(Long advertiserId, String advertiserName, String phoneNumber, String location, String profession, String description, String price) {
        this.advertiserId = advertiserId;
        this.advertiserName = advertiserName;
        this.phoneNumber = phoneNumber;
        this.location = location;
        this.profession = profession;
        this.description = description;
        this.price = price;
        this.status = AdStatus.ACTIVE;


        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        this.dateCreated = myDateObj.format(myFormatObj);
        this.craftsmenServiceClients = new ArrayList<>();
        this.isEnabled = false;
    }
}
