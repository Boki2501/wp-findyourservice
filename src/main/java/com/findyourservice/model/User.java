package com.findyourservice.model;

import com.findyourservice.model.enumerations.AuthProvider;
import com.findyourservice.model.enumerations.JobProfession;
import com.findyourservice.model.enumerations.Role;
import com.sun.istack.NotNull;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Data
@Entity
@Table(name = "service_users")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Email
    @NotNull
    private String email;
    private String username;
    private String password;
    private String name;
    private String surname;

    private String phone_number;
    private String city;
    private String country;
    private double rating;

    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    private String profileImg;


    @Enumerated(value = EnumType.STRING)
    private JobProfession profession;

    private boolean isAccountNonExpired = true;
    private boolean isAccountNonLocked = true;
    private boolean isCredentialsNonExpired = true;
    private boolean isEnabled = false;



    @Enumerated(value = EnumType.STRING)
    private Role role;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider provider;


    @ManyToMany(mappedBy = "passengers",cascade = CascadeType.PERSIST)
    private List<Trips> trips;

    @ManyToMany(mappedBy = "foodClients",cascade = CascadeType.PERSIST)
    private List<Food> foodServices;

    @ManyToMany(mappedBy = "educationClients",cascade = CascadeType.PERSIST)
    private List<Educations> educationServices;

    @ManyToMany(mappedBy = "craftsmenServiceClients",cascade = CascadeType.PERSIST)
    private List<Craftsmen> craftsmenServices;


    public User(){}

    public User(@Email String email, String password, String name, String surname, String phone_number, String city, String country,String profileImg, JobProfession profession) {
        this.email = email;
        this.password = password;
        this.username = name+" "+surname;
        this.name = name;
        this.surname = surname;
        this.phone_number = phone_number;
        this.city = city;
        this.country = country;
        this.profileImg=profileImg;
        this.profession = profession;
        this.role = Role.ROLE_USER;
        this.provider = AuthProvider.local;
        this.rating=0;
    }

    @Override
    public String  toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(role);
    }

    @Override
    public String getUsername() {
        return this.name+" "+this.surname;
    }

    public String getFullname() {
        return this.name+" "+this.surname;
    }

    public String getLocation(){
        return this.city+", "+this.country;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public double getRating(){
        return Double.parseDouble(String.format("%.2f",this.rating));
    }
}
