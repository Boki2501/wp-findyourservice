package com.findyourservice.model;

import com.findyourservice.model.enumerations.ServiceType;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Ratings {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long fromUserId;
    private Long toUserId;
    private Long serviceId;
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;
    private int rate;
    private String comment;


    public Ratings(){}

    public Ratings(Long fromUserId, Long toUserId, Long serviceId, ServiceType serviceType, int rate, String comment) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.serviceId = serviceId;
        this.serviceType = serviceType;
        this.rate = rate;
        this.comment = comment;
    }
}
