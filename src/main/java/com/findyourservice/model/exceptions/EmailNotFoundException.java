package com.findyourservice.model.exceptions;

public class EmailNotFoundException extends RuntimeException{
    public EmailNotFoundException(String email){
        super(String.format("User with given email %s not found.",email));
    }
}
