package com.findyourservice.model.exceptions;

public class CraftsmenServiceNotFound extends RuntimeException{
    public CraftsmenServiceNotFound(Long id){
        super(String.format("Craftsmen service with given id: %s was not found.",id));
    }
}
