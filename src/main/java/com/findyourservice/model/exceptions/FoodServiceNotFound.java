package com.findyourservice.model.exceptions;

public class FoodServiceNotFound extends RuntimeException{
    public FoodServiceNotFound(Long id){
        super(String.format("Food service with given id: %s was not found.",id));
    }
}
