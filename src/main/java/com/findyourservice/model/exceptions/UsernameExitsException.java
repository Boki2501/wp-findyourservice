package com.findyourservice.model.exceptions;

public class UsernameExitsException extends RuntimeException{
    public UsernameExitsException(String username) {
        super(String.format("User with username %s already exists",username));
    }
}
