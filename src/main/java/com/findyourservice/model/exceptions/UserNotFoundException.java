package com.findyourservice.model.exceptions;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(Long id){
        super(String.format("User with given id: %s, was not found.",id));
    }
}
