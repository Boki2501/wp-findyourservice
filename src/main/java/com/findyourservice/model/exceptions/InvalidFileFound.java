package com.findyourservice.model.exceptions;

public class InvalidFileFound extends RuntimeException{
    public InvalidFileFound(){
        super("Invalid file found");
    }
}
