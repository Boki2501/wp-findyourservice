package com.findyourservice.model.exceptions;

public class EducationServiceNotFound extends RuntimeException{
    public EducationServiceNotFound(Long id){
        super(String.format("Education service with given id: %s was not found.",id));
    }
}
