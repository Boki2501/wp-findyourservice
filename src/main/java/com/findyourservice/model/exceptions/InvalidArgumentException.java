package com.findyourservice.model.exceptions;

public class InvalidArgumentException extends  RuntimeException{
    public InvalidArgumentException(){
        super("Invalid argument exception");
    }
}
