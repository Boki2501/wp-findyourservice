package com.findyourservice.model.exceptions;

public class AccountNotEnabledException extends RuntimeException{
    public AccountNotEnabledException(){
        super("Your account is not enabled, wait for administrator approval!");
    }
}
