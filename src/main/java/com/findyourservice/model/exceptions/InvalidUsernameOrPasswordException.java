package com.findyourservice.model.exceptions;

public class InvalidUsernameOrPasswordException extends RuntimeException {

    public InvalidUsernameOrPasswordException(){
        super(String.format("Invalid username or password!"));
    }
}
