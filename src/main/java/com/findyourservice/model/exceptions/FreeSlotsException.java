package com.findyourservice.model.exceptions;

public class FreeSlotsException extends RuntimeException{
    public FreeSlotsException(){
        super("No more free slots available!");
    }
}
