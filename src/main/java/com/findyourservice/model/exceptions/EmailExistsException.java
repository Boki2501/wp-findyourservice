package com.findyourservice.model.exceptions;

public class EmailExistsException  extends  RuntimeException{
    public EmailExistsException(String email){
        super(String.format("Email %s already exists",email));
    }
}
