package com.findyourservice.model.exceptions;

public class TripNotFoundException extends RuntimeException{
    public TripNotFoundException(Long id){
        super(String.format("Trip with id: %s, was not found!",id));
    }
}
