package com.findyourservice.config;

import com.findyourservice.model.enumerations.EducationServiceType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class EducationServiceTypeConverter implements Converter<String, EducationServiceType> {

    @Override
    public EducationServiceType convert(String s) {
        return EducationServiceType.valueOf(s);
    }
}
