package com.findyourservice.security.oauth2;

import com.findyourservice.model.User;
import com.findyourservice.model.enumerations.AuthProvider;
import com.findyourservice.model.exceptions.EmailNotFoundException;
import com.findyourservice.security.oauth2.user.CustomOAuth2User;
import com.findyourservice.service.UserService;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


@Component
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final UserService userService;

    public OAuth2AuthenticationSuccessHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        CustomOAuth2User customOAuth2User = (CustomOAuth2User) authentication.getPrincipal();
        String email = customOAuth2User.getEmail();
        String name = customOAuth2User.getName();
        AuthProvider authProvider = customOAuth2User.getAuthProvider();
        Optional<User> user = userService.loadByEmail(email);


            if (user.isEmpty()) {
                //register new user
                userService.registerNewUserAfterOAuthLoginSuccess(email, name, authProvider);

            } else {
                //update existing
                userService.updateExistingUserAfterOAuthLoginSuccess(user.get(),name, authProvider);
            }




        super.onAuthenticationSuccess(request, response, authentication);
    }
}