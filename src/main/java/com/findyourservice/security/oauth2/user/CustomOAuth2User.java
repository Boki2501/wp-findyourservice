package com.findyourservice.security.oauth2.user;

import com.findyourservice.model.enumerations.AuthProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.Map;

public class CustomOAuth2User implements OAuth2User {

    private OAuth2User oAuth2User;
    private AuthProvider authProvider;

    public CustomOAuth2User(OAuth2User oAuth2User,String provider) {

        this.oAuth2User = oAuth2User;
        if(provider.equals("facebook"))
            this.authProvider=AuthProvider.facebook;
        else
            this.authProvider=AuthProvider.google;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return this.oAuth2User.getAttributes();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.oAuth2User.getAuthorities();
    }

    @Override
    public String getName() {
        return this.oAuth2User.getAttribute("name");
    }

    public String getFullname(){
        return this.oAuth2User.getName();
    }

    public String getEmail(){ return this.oAuth2User.getAttribute("email");}

    public AuthProvider getAuthProvider(){
        return this.authProvider;
    }

}
