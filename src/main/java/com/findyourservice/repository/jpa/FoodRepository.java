package com.findyourservice.repository.jpa;

import com.findyourservice.model.Food;
import com.findyourservice.model.enumerations.AdStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FoodRepository extends JpaRepository<Food,Long> {

    List<Food> findAllByStatus(AdStatus status);
}
