package com.findyourservice.repository.jpa;

import com.findyourservice.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByUsernameAndPassword(String username, String password);

    Optional<User> findByUsername(String email);
    Optional<User> findByEmail(String email);
    Boolean existsByEmail(String email);
    void deleteUserById(Long id);

    Optional<User> findUserById(Long id);

    @Query("Select u.trips from User u WHERE u.id=:id")
    List<Trips> findAllTripsByUserId(@Param("id")Long id);
    @Query("Select u.foodServices from User u where u.id=:id")
    List<Food> findAllFoodServicesByUserId(@Param("id")Long id);
    @Query("Select u.educationServices from User u where u.id=:id")
    List<Educations> findAllEducationServicesByUserId(@Param("id")Long id);
    @Query("Select u.craftsmenServices from User u where u.id=:id")
    List<Craftsmen> findAllCraftsmenServicesByUserId(@Param("id")Long id);
}
