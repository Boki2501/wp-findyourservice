package com.findyourservice.repository.jpa;

import com.findyourservice.model.Trips;
import com.findyourservice.model.enumerations.AdStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TripsRepository extends JpaRepository<Trips,Long> {
    List<Trips> findAllByStatus(AdStatus adStatus);
}
