package com.findyourservice.repository.jpa;

import com.findyourservice.model.Educations;
import com.findyourservice.model.enumerations.AdStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EducationsRepository extends JpaRepository<Educations,Long> {
    List<Educations> findAllByStatus(AdStatus adStatus);
}
