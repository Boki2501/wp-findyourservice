package com.findyourservice.repository.jpa;

import com.findyourservice.model.Craftsmen;
import com.findyourservice.model.enumerations.AdStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CraftsmenRepository extends JpaRepository<Craftsmen,Long> {
    List<Craftsmen> findAllByStatus(AdStatus status);
}
