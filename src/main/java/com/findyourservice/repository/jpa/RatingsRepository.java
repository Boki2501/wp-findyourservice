package com.findyourservice.repository.jpa;

import com.findyourservice.model.RateComments;
import com.findyourservice.model.Ratings;
import com.findyourservice.model.enumerations.ServiceType;
import javassist.compiler.ast.Pair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingsRepository extends JpaRepository<Ratings,Long> {
    @Query("Select r.rate from Ratings r where r.toUserId=:id")
    List<Integer> findAllRatingsGivenToUserId(Long id);


    @Query("Select r.serviceId from Ratings r " +
            "where r.fromUserId=:id and r.serviceType=:serviceType")
    List<Long> findAllServicesRatedFromUserId(Long id, ServiceType serviceType);


    @Query("Select r.comment from Ratings r where r.toUserId=:id")
    List<String> findAllCommentsGivenToUser(Long id);

    @Query ("Select new com.findyourservice.model.RateComments(r.rate, r.comment) from Ratings r where r.toUserId=:id")
    List<RateComments> findAllRatesAndCommentGivenToUser(Long id);
}
